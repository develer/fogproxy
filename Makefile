SHELL=/bin/bash -o pipefail

all: release

install: requirements.txt.done

requirements.txt.done:
	pip install -r requirements.txt
	touch $@

develop: requirements-dev.txt.done install

requirements-dev.txt.done:
	pip install -r requirements-dev.txt
	touch $@

lint: develop flake8.results

flake8.results: proxy.py
	flake8 $^
	touch $@

mypy: develop proxy.py
	mypy --py2 --silent-imports proxy.py

release: build DESCRIPTION.rst

DESCRIPTION.rst: README.md
	pandoc -f markdown -t rst -o $@ $^

build: lint
	python setup.py bdist_wheel

dist: lint dist/fogproxy

dist/fogproxy: develop proxy.py
	pyinstaller --onefile proxy.py --name fogproxy

.PHONY: install develop build mypy lint
