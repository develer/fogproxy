#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2016 Matteo Bertini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hashlib
import json
import logging
import re
import signal
import tempfile
import xml.etree.ElementTree as ET
from io import BytesIO
from typing import List, Tuple, Dict  # noqa: mypy

import click
import requests
from dirq.QueueSimple import QueueSimple
import gunicorn  # noqa: pleceholder for pyinstaller
from gunicorn import glogging  # noqa: pleceholder for pyinstaller
from gunicorn.workers import sync  # noqa: pleceholder for pyinstaller

from bottle import Bottle, request

try:
    from pathlib2 import Path
except ImportError:
    from pathlib import Path

######################################################################
# Globals
######################################################################

HERE = Path(__file__).parent

######################################################################
# `scoutSubmit.asp` extension with attachments
######################################################################


def dump_error(errors_path, parts):
    with tempfile.NamedTemporaryFile(mode='w+',
                                     prefix='err',
                                     suffix='.foz',
                                     delete=False,
                                     dir=str(errors_path)) as dump:
        pdump = Path(dump.name)
        ddump = pdump.with_name(pdump.stem)
        files = []
        if parts['files']:
            ddump.mkdir()
            for file in parts['files']:
                attachment = ddump / file.filename
                with attachment.open('w+b') as f:
                    file.save(f)
                files.append(str(attachment.relative_to(errors_path.resolve())))
        json.dump({'forms': parts['forms'], 'files': files}, dump)
    return dump.name, parts['forms']['sScoutDescription']


def prepare_dump(request):
    forms = dict(request.forms)
    forms.pop(None, None)
    forms['sScoutDescription'] = re.sub('\d{4,}', '<DIGITS>', forms['Description'])
    files = request.files.values()
    return {'forms': forms, 'files': files}


def get_scout_message(messages_path, scout_desc):
    message = scout_desc.encode('utf8')
    message_hash = hashlib.md5(message).hexdigest()
    message_cache = (messages_path / message_hash).with_suffix('.message')
    if message_cache.exists():
        return message_cache.read_text('utf8')
    else:
        return None


def bottle_app(errors_path, messages_path):
    app = Bottle()
    errors_queue = QueueSimple(str(errors_path / 'queue'))

    @app.route('/scoutSubmit.asp', method='POST')
    def save_report():
        name, scout_desc = dump_error(errors_path, prepare_dump(request))
        message = get_scout_message(messages_path, scout_desc)
        content = ET.Element("Success")
        if message is not None:
            content.text = message
        # enqueue error for upload
        errors_queue.add(name)
        return ET.tostring(content, encoding='utf8')

    return app

######################################################################
# Use full fogbugz api to push the reported error and save the message
######################################################################


def load_error(errors_path, name):
    # type: (Path, str) -> Tuple[Dict, List[Path]]
    with open(name) as f:
        parms = json.loads(f.read())
    forms = parms['forms']
    files = [Path(errors_path / filename) for filename in parms['files']]
    return forms, files


def prepare_request(forms, files, api, token):
    data = {'token': token, 'cmd': 'new'}
    name_map = {'sScoutDescription': 'sScoutDescription',
                'ScoutUserName': 'sPersonEditedBy',
                'ScoutProject': 'sProject',
                'ScoutArea': 'sArea',
                'Description': 'sTitle',
                'Extra': 'sEvent'}
    for scout_name in (n for n in name_map if n in forms):
        data[name_map[scout_name]] = forms[scout_name]
    attachments = {'File%d' % (c + 1): (file.name, BytesIO(file.read_bytes()))
                   for c, file in enumerate(files)}
    if len(attachments) > 1:
        data['nFileCount'] = len(attachments)
    return data, attachments


def push_error(errors_path, messages_path, api, token, name):
    forms, files = load_error(errors_path, name)
    data, attachments = prepare_request(forms, files, api, token)
    r = requests.post(api, data=data, files=attachments)
    if r.status_code == requests.codes.ok:
        cache_scout_message(messages_path, api, token, data['sScoutDescription'])


def cache_scout_message(messages_path, api, token, scout_desc):
    params = {'token': token, 'cmd': 'listScoutCase', 'sScoutDescription': scout_desc}
    r = requests.get(api, params=params)
    if r.status_code == requests.codes.ok:
        response = ET.XML(r.content)
        message_tag = response.find('*sScoutMessage')
        if message_tag is not None and message_tag.text is not None:
            message = scout_desc.encode('utf8')
            message_hash = hashlib.md5(message).hexdigest()
            message_cache = messages_path / message_hash
            if not message_cache.with_suffix('.message').exists():
                message_cache.write_text(message_tag.text, encoding='utf8')
                message_cache.rename(message_cache.with_suffix('.message'))


def consume_errors(errors_path, messages_path, api, token, log):
    errors_queue = QueueSimple(str(errors_path / 'queue'))
    errors_done = QueueSimple(str(errors_path / 'done'))
    log.debug("Consuming queue [%d]...", errors_queue.count())

    def log_rmtree_error(function, path, excinfo):
        log.error("Error removing dump data %s(%r): %s", function.__name__, path, excinfo[1])

    def process_error(item):
        name = errors_queue.get(item)
        push_error(errors_path, messages_path, api, token, name)
        errors_queue.remove(item)
        return name.decode()

    for item in (i for i in errors_queue if errors_queue.lock(i)):
        try:
            log.debug("Pushing error %r", item)
            name = process_error(item)
            errors_done.add(name)
        except:
            log.exception("Error uploading error %r", item)
        finally:
            errors_queue.unlock(item, permissive=True)
    errors_queue.purge()
    log.debug("Queue consumed!")


def cleanup_done_errors(errors_path, log):
    log.info("Cleaning up uploaded errors...")
    errors_done = QueueSimple(str(errors_path / 'done'))

    def remove_files(name):
        forms, files = load_error(errors_path, name)
        for file in files:
            file.unlink()
        pdump = Path(name.decode())
        pdump.unlink()
        ddump = pdump.with_name(pdump.stem)
        try:
            ddump.rmdir()
        except:
            log.error("Unable to remove %s", ddump)

    for item in (i for i in errors_done if errors_done.lock(i)):
        name = errors_done.get(item)
        try:
            remove_files(name)
            errors_done.remove(item)
        except:
            errors_done.unlock(item, permissive=True)
    errors_done.purge()

######################################################################
# Utils
######################################################################


def create_folders(spool_dir):
    errors = spool_dir / 'errors'
    if not errors.exists():
        errors.mkdir()
    messages = spool_dir / 'messages'
    if not messages.exists():
        messages.mkdir()
    return {'errors': errors, 'messages': messages}


def setup_logging(level):
    logger = logging.getLogger('fogproxy')
    logger.setLevel(level)

    # Do not propagate logging messages
    logger.propagate = False

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

######################################################################
# Command line
######################################################################


@click.group()
@click.option('--spool-dir',
              default=str(HERE),
              envvar='FOGPROXY_SPOOL_DIR',
              type=click.Path(writable=True),
              help='Data folder (default: executable folder) or FOGPROXY_SPOOL_DIR env var')
@click.pass_context
def cli(ctx, spool_dir):
    ctx.obj['spool_dir'] = spool_dir = Path(spool_dir)
    ctx.obj.update(**create_folders(spool_dir))


@cli.command()
@click.argument('apiurl')
@click.argument('token')
@click.option('--loop/--no-loop', default=False, help='Loop and consume queue on SIGHUP')
@click.pass_context
def upload(ctx, apiurl, token, loop):
    log = logging.getLogger('fogproxy.upload')
    log.info("Starting uploader...")

    def _consume_errors(*args):
        errors_path = ctx.obj['errors']
        messages_path = ctx.obj['messages']
        consume_errors(errors_path, messages_path, apiurl, token, log)

    signal.signal(signal.SIGHUP, _consume_errors)

    def gracefull_term(*args):
        raise KeyboardInterrupt

    signal.signal(signal.SIGTERM, gracefull_term)

    try:
        _consume_errors()
        while loop:
            signal.pause()
    except (KeyboardInterrupt, SystemError):
        pass

    log.info("Exiting from uploader!")


@cli.command()
@click.option('--host', envvar='FOGPROXY_HOST', default='localhost',
              help='Serve on provided host (env: FOGPROXY_HOST)')
@click.option('--port', envvar='FOGPROXY_PORT', default=8000,
              help='Use provided port (env: FOGPROXY_PORT)')
@click.pass_context
def serve(ctx, host, port):
    log = logging.getLogger('fogproxy.serve')
    log.info("Starting proxy...")
    errors_path = ctx.obj['errors']
    messages_path = ctx.obj['messages']
    # bottle gracefully handles a KeyboardInterrupt exception
    app = bottle_app(errors_path, messages_path)
    app.run(host=host, port=port, debug=__debug__, server='gunicorn', workers=4)
    log.info("Exiting from proxy!")


@cli.command()
@click.pass_context
def cleanup(ctx):
    log = logging.getLogger('fogproxy.cleanup')
    log.info("Starting cleanup...")
    errors_path = ctx.obj['errors']
    cleanup_done_errors(errors_path, log)
    log.info("Done!")


def main():
    setup_logging(logging.DEBUG)
    cli(obj={})


if __name__ == '__main__':
    main()
